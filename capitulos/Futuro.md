# Futuro

O período Entre Guerras, das décadas de 1920 a 1940, foi caracterizado por uma sociedade em pedaços no mundo ocidental. As mazelas da Primeira Guerra Mundial somaram-se à crise financeira mundial e à ascensão de regimes totalitários de direita e esquerda em todo o globo.
A despeito dos avanços tecnológicos ocorridos entre o final do século XIX e começo do século XX, a sociedade enxergava um futuro sombrio, marcado pela automatização do ser humano e até mesmo sua escravização - fosse por máquinas, alienígenas ou mesmo líderes radicais e despóticos.
Nesse contexto, inovações e avanços eram vistos como uma tentativa direta de dominação - apenas instrumentos para criar poder e relegar o ser humano comum à perda de individualidade e identidade.
O romance Admirável Mundo Novo é um claro exemplo dessa expectativa Aldous Huxley cria, na obra, uma sociedade futurista e distópica, na qual a reprodução humana é automatizada e geneticamente controlada em nível tecnológico, e a reprodução convencional é vista como uma heresia, tal qual crenças e religiões
[1].

O livro 1984, embora posterior à Segunda Guerra Mundial, segue linha semelhante, mas de maneira ainda mais sombria, como resultado do avanço tecnológico como forma de controle. A obra mostra uma sociedade distópica na qual membros são vigiados e supervisionados ininterruptamente por uma força que estende o poder da classe dominante até a vida privada e o cotidiano de todos, por intermédio do "Grande Irmão" - que poderia ser interpretado como uma mescla de poder despótico e inteligência artificial
O cinema também cedeu à visão apocalíptica do futuro. Filmes como Metropolis, de 1927, já mostravam a forma como a expectativa de futuro era o subjugar da sociedade aos desejos de uma classe dominante, que congregava todo o poder financeiro e político, orientando a evolução tecnológica à aplicação e manutenção do poder. Já em 1927 conceitos como a inteligência artificial e os simulacros eram discutidos e cogitados, porém no sentido de perpetuar oligarquias no topo da sociedade, nunca de seu benefício direto.
Os exemplos vão além, mas o fato é que, durante a primeira metade do século passado, a inovação era certamente vista com desconfiança pela sociedade. A aplicação de novos conceitos esbarrava não apenas na dúvida, mas nos mais profundos medos de cada uma das pessoas.[1].

A tecnologia continuaria a avançar, não obstante, por conta de projetos militares e industriais principalmente ligados à polarização da política mundial. Sua migração para a sociedade, embora hoje vista como tendo sido uma "privação" por parte de governantes, na verdade tornou-se difícil e encontrava barreiras no próprio imaginário popular
Computadores levaram três décadas para ganhar confiança e desempenhar algum papel na vida do homem comum, e mesmo a televisão foi recebida com reservas em seus primeiros anos de mercado. A lenta popularização impedia o ganho de escala e, consequentemente, atrasou a disseminação de tecnologias que foram dominadas ainda durante a Segunda Guerra, mas chegariam às nossas casas muitas décadas depois.[1].



# Referências:

1. André Telles, O futuro é smart: Como as novas tecnologias estão redesenhando os negócios e o mundo em que vivemos. PUCPRess, 2018

[Anterior](../README.md)


