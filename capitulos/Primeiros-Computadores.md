# Os Primórdios da Computação

Apesar dos computadores eletrônicos terem efetivamente aparecido somente na década de 40, os fundamentos em que se baseiam remontam a centenas ou até mesmo milhares de anos.

Se levarmos em conta que o termo computar significa fazer cálculos, contar, efetuar operações aritméticas, computador seria então o mecanismo ou máquina que auxilia essa tarefa, com vantagens no tempo gasto e na precisão. Inicialmente o homem utilizou seus próprios dedos para essa tarefa, dando origem ao sistema decimal e aos termos digital e digito. Para auxílio deste método, eram usados gravetos, contas ou marcas na parede.

A partir do momento que o homem pré-histórico trocou seus hábitos nômades por aldeias e tribos fixas, desenvolvendo a lavoura, tornou-se necessário um método para a contagem do tempo, delimitando as épocas de plantio e colheita.

Tábuas de argila foram desenterradas por arqueólogos no Oriente Médio, próximo à Babilônia, contendo tabuadas de multiplicação e recíprocos. Acredita-se que tenham sido escritas por volta de 1700 a.C. e usavam o sistema sexagesimal (base 60), dando origem às nossas atuais unidades de tempo.

## Primeiros computadores

Em 1890, o norte americano Hermann Hollerith (1860-1929) desenvolve o primeiro computador mecânico. A partir de 1930, começam as pesquisas para substituir as partes mecânicas por elétricas. O Mark I, concluído em 1944 por uma equipe liderada por Howard Aiken, é o primeiro computador eletromecânico capaz de efetuar cálculos mais complexos sem a interferência humana. Ele mede 15 m x 2,5 m e demora 11 segundos para executar um cálculo. Em 1946, surge o Eniac (Electronic Numerical Integrator and Computer), primeiro computador eletrônico e digital automático: pesa 30 toneladas, emprega cerca de 18 mil válvulas e realiza 4.500 cálculos por segundo. O Eniac contém a arquitetura básica de um computador, empregada até hoje: memória principal (área de trabalho), memória auxiliar (onde são armazenados os dados), unidade central de processamento (o "cérebro" da máquina, que executa todas as informações) e dispositivos de entrada e saída de dados que atualmente permitem a ligação de periféricos como monitor, teclado, mouse, scanner, tela, impressora, entre outros. A invenção do transistor, em 1947, substitui progressivamente as válvulas, aumentando a velocidade das máquinas.

## Os Computadores Pessoais

O tamanho e o preço dos computadores começam a diminuir a partir da década de 50. Neste período, inicia-se a pesquisa dos circuitos integrados, os chips , responsáveis pela crescente miniaturização dos equipamentos eletrônicos. Em 1974, a Intel projeta o microprocessador - dispositivo que reúne num mesmo chip, todas as funções do processador central - tecnologia que permite a criação do computador pessoal, ou microcomputador. O primeiro computador pessoal é o Apple I, inventado em 1976 pelos americanos Steve Jobs (1955-) e Stephan Wozniak.

Em 1981, a IBM lança o seu PC (Personal Computer), que se torna um sucesso comercial. O sistema operacional usado é o MS-DOS, desenvolvido pela empresa de softwares Microsoft. Na época, Bill Gates , o dono da Microsoft, convence a IBM e as demais companhias a adotarem o sistema operacional de sua empresa. Isso permite que um mesmo programa funcione em micros de diversos fabricantes. Posteriormente, os PCs passam a usar microprocessadores cada vez mais potentes: 286, 386SX, 386DX, 486SX, 486DX. O Pentium, que surge nos anos 90, é atualmente o processador mais avançado usado em PCs.

O único micro a fazer frente aos PCs é o Macintosh, lançado em 1984, que revoluciona o mercado ao promover o uso de ícones e do mouse. No ano seguinte, a Microsoft lança a interface gráfica Windows, adaptando para os PCs o uso de ícones e do mouse. O Windows só alcança sucesso a partir de 90, com a versão 3.0. A nova versão, lançada em 1995, totaliza 45,8 milhões de usuários registrados pela Microsoft em dezembro de 1996.       

Na década de 90 surgem os computadores que, além do processamento de dados, reúnem fax, modem, secretária eletrônica, scanner, acesso à Internet e drive para CD-ROM. Os CDs-ROM, sigla de compact disc read-only memory, criados no início da década, são discos a laser que armazenam até 650 megabytes, 451 vezes mais do que um disquete (1,44 megabytes). Além de armazenar grande quantidade de texto, o CD-ROM tem capacidade de arquivar fotos, vídeos e animações. Em 1996 é anunciado o lançamento do DVD (digital vídeo disc), que nos próximos anos deve substituir o CD-ROM e as fitas de videocassete. O DVD é um compact-disc com capacidade de 4,7 gigabytes (cerca de 7 CDs-ROM). Segundo os fabricantes, terá a capacidade de vídeo de um filme de 135 minutos em padrão de compressão MPEG (tela cheia) e alta qualidade de áudio. Terá o mesmo diâmetro e espessura dos CDs atuais, mas será reproduzido em um driver específico, que também poderá ser ligado à televisão. Alguns CDs-ROM são interativos, ou seja, permitem que o usuário controle, à vontade, a navegação pelo seu conteúdo. Os computadores portáteis (laptops e palmtops), marcas da miniaturização da tecnologia, também se popularizam nos anos 90.

## Referências

Gadelha.J http://www.ic.uff.br/~aconci/evolucao.html . Acesso em: 22 de outubro de 2021)

[Anterior](../README.md)
