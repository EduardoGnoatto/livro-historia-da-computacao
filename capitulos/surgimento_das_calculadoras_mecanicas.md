# Calculadoras Mecânicas

À medida que sociedade evoluiu foi notável a necessidade de rapidez e agilidade para tarefas do cotidiano, com isto em mente Wilhelm Schickard, em 1623 projetou uma das invenções mais utilizadas pela humanidade até os tempos atuais: a calculadora mecânica. Tal invenção era capaz de somar, subtrair, multiplicar e dividir números de até 6 dígitos, o resultado era indicado pelo toque de um sino o que deixou a calculadora conhecida como “relógio de calcular”.[1].

[Reprodução da calculadora de Wilhelm Schickard.](imagens/calculadora_mecanica.jpg)

Infelizmente o único exemplar da calculadora de Wilhelm foi destruído em um incêndio, porém as anotações do projeto foram encontradas, possibilitando sua reprodução. Antes de tais rascunhos serem encontrados a invenção era atribuída ao francês Blaise Pascal, pela criação do Pascaline.[1].

A pascaline foi uma calculadora construída em 1642. tinha como elemento essencial uma roda dentada construída com 10 "dentes". Cada "dente" corresponde a um algarismo, de 0 a 9. A primeira roda da direita corresponde às unidades, a segunda a sua esquerda corresponde às dezenas, a seguinte às centenas e assim sucessivamente. Pascal recebeu a patente do rei da França para criar mais Pascalines e vender no mercado. Ele construiu 50 versões dessa calculadora, mas ela não teve muita aceitação no mercado.[1].

No fim do século XIX e início do século XX, as calculadoras eram objetos de uso bastante restrito. Foi nos anos seguintes, com a criação de máquinas cada vez menores e mais baratas que a calculadora se transformou no popular instrumento que conhecemos atualmente.[2]. 



# Referências:

1. https://www.engquimicasantossp.com.br/2012/09/historia-da-calculadora.html

2. https://www.historiadetudo.com/calculadora

[Anterior](../README.md)
