# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/Primeiros-Computadores.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Primeira Geração](capitulos/E_dos_CP_e_sua_I_Primeira_Geração.md)
1. [Computação Móvel](capitulos/Computação-Móvel.md)
1. [Futuro](capitulos/Futuro.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168497/avatar.png?width=400)  | Eduardo José Gnoatto | EduardoGnoatto | [eduardognoatto@alunos.utfpr.edu.br](mailto:eduardognoatto@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/5658313/avatar.png?width=400)  | Lucas Fortes Zillmer | zillmxr | [lucasforteszillmer@alunos.utfpr.edu.br](mailto:lucasforteszillmer@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9926355/avatar.png?width=400)  | Vithor Augusto Mohr | vithormohr | [vit.mohr@gmail.com](mailto:vit.mohr@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/10042851/avatar.png?width=400)  | Bruno Antunes da Silva Capim | Bruno Capim | [[brunocapim.2021@alunos.utfpr.edu.br]([brunocapim.2021@alunos.utfpr.edu.br)

